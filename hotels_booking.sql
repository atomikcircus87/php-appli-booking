-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Dim 26 Avril 2015 à 19:34
-- Version du serveur: 5.6.12-log
-- Version de PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `hotels_booking`
--
CREATE DATABASE IF NOT EXISTS `hotels_booking` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `hotels_booking`;

-- --------------------------------------------------------

--
-- Structure de la table `booking`
--

CREATE TABLE IF NOT EXISTS `booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_guest` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `name_hotel` varchar(50) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `dt_begin` date NOT NULL,
  `dt_end` date NOT NULL,
  `room_number` int(11) NOT NULL,
  `dt_creation` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Structure de la table `hotels`
--

CREATE TABLE IF NOT EXISTS `hotels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_hotels` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `number_room` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `hotels`
--

INSERT INTO `hotels` (`id`, `name_hotels`, `city`, `number_room`) VALUES
(1, 'Le pont Loyal', 'Nantes', 30),
(2, 'Le Vendame', 'Lyon', 27),
(3, 'Le Brastol', 'Paris', 32),
(4, 'Georges III', 'Lille', 25),
(5, 'Le Rutz', 'Bayonne', 35);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
