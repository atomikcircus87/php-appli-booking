<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Site de réservations de chambres d'hôtels</title>

<link href="../css/index.css" rel="stylesheet" />

</head>

<body>

	<main >

		<header>
			<h1 class="title">Site de réservations de chambres d'hôtels</h1>
		</header>
		
		<nav>
			<li><a href="../index.php">Accueil</a></li>
			<li><a href="../page_hotels/page_hotels.php">Liste des Hôtels</a></li>
			<li><a href="#">Formulaire de Booking</a></li>
			<li><a href="../page_admin/page_admin.php">Administration</a></li>
		</nav>

		<article>
			<h2 class="welcome">Formulaire de booking</h2>
			
			<section>
			
				<form method="post" action="#">
				
					<fieldset>
					
						<legend>Formulaire de réservation</legend>
						
						<p>Veuillez entrer votre nom:</p>
							<input type="texte" name="name">
						
						<p>Veuillez entrer votre adresse e-mail:</p>
							<input type="texte" name="email">
						
						<p>Veuillez choisir le nom de l'hôtel où réserver une chambre:</p>
						
							<select name="selHotel">
								<?php
								require('../process/process_classBooking.php');
								require('../process/process_selhotels.php');
								?>		
							</select>

						<p>Veuillez entrer le jour de début de réservation (AAAA-MM-JJ):</p>
							<input type="date" name="dt_begin">
							
						<p>Veuillez entrer le jour de fin de réservation (AAAA-MM-JJ):</p>
							<input type="date" name="dt_end"></br>
							
							</br>
							
							<input type="submit" name="submitForm" value="Valider">
							<?php
							require('../process/process_booking.php');
							?>
						
					</fieldset>
					
				</form>
				
			</section>
			
		</article>    
		
	</main>
	
</body>

</html>
