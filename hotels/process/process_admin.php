<?php
require('../class/Admin.php');
require('../class/AdminManager.php');

$db=new PDO ('mysql:host=localhost;dbname=hotels_booking','root','');

//-----------------------------------------------------------------VALIDATION DU FORMULAIRE AJOUT-----------------------------------------------------------------//
if(isset($_POST['submitFormAddHotel'])){
	if(!empty($_POST['nameHotel']) AND !empty($_POST['cityHotel']) AND !empty($_POST['numberRoom'])){
		
		$admin_data=array('id'=>'1', 'nameHotel'=>$_POST['nameHotel'],'cityHotel'=>$_POST['cityHotel'],'numberRoom'=>$_POST['numberRoom']);
		
		try{
		$admin=new Admin($admin_data);
		
		$manager=new AdminManager($db);
		$manager->addHotel($admin);
		
			}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}
	else
	{
	echo "Veuillez compléter tous les champs du formulaire !";
	}
}

//-----------------------------------------------------------------VALIDATION DU FORMULAIRE SUPPRESSION-----------------------------------------------------------------//

if(isset($_POST['submitFormDeleteHotel'])){

	$admin_data=array('id'=>'1', 'nameHotel'=>$_POST['selDelHotel'],'cityHotel'=>'null','numberRoom'=>'1');
	
	try{
		$admin=new Admin($admin_data);
		
		$manager=new AdminManager($db);
		$manager->deleteHotel($admin);
		
			}
		catch(Exception $e){
			echo $e->getMessage();
		}
}

//-----------------------------------------------------------------VALIDATION DU FORMULAIRE AFFICHAGE-----------------------------------------------------------------//

if(isset($_POST['submitFormListHotel'])){

	$admin_data=array('id'=>'1', 'nameHotel'=>$_POST['selListHotel'],'cityHotel'=>'null','numberRoom'=>'1');
	
	try{
		$admin=new Admin($admin_data);
		
		$manager=new AdminManager($db);
		$manager->getListBooginkHotel($admin);
		
			}
		catch(Exception $e){
			echo $e->getMessage();
		}
}	