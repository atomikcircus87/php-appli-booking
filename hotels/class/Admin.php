<?php

Class Admin {

	protected $_id;
	protected $_nameHotel;
	protected $_cityHotel;
	protected $_numberRoom;
	
	protected static $error;
	
	const MSG_ERROR_NAME='Veuillez entrer une chaine de caractéres.';
	const MSG_ERROR_NUMBER='Veuillez rentrer un nombre de chambres.';
	const MSG_ERROR_END='<br>La création de la ville ne peut pas être créé.';
	
public function __construct(array $data){
		$this->setId($data['id']);
		$this->setNameHotel($data['nameHotel']);
		$this->setCityHotel($data['cityHotel']);
		$this->setNumberRoom($data['numberRoom']);
		
		if(!empty(self::$error)){
			throw new Exception(self::$error.self::MSG_ERROR_END);
		}
}

//-----------------------------------------------------------------SETTERS-----------------------------------------------------------------//

public function setId($id){
	if((is_numeric($id)) AND ($id>0)){
	$this->_id=$id;
	}
}

public function setNameHotel($nameHotel){
	if(is_string($nameHotel)){ 
	$this->_nameHotel=$nameHotel;
	}
	else
	{
		$this->setError(self::MSG_ERROR_NAME);
	}
}

public function setCityHotel($cityHotel){
	$this->_cityHotel=$cityHotel;
}

public function setNumberRoom($numberRoom){
	if((is_numeric($numberRoom)) AND ($numberRoom>0)){
	$this->_numberRoom=$numberRoom;
	}
	else
	{
		$this->setError(self::MSG_ERROR_NUMBER);
	}
}

public function setError($msg){
	self::$error=$msg;
	}
	
//-----------------------------------------------------------------GETTERS-----------------------------------------------------------------//

public function getId(){
	return $this->_id;
	}

public function getNameHotel(){
	return $this->_nameHotel;
	}

public function getCityHotel(){
	return $this->_cityHotel;
	}

public function getNumberRoom(){
	return $this->_numberRoom;
	}

public function getError(){
	return self::$error;
	}
}