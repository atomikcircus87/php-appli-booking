<?php

Class BookingManager {

	private $_db;
	
	public function __construct($db){
			$this->setDb($db);
	}
	
	public function setDb(PDO $db){
		$this->_db=$db;
	}
	
//-----------------------------------------------------------------METHOD ADD BOOKING-----------------------------------------------------------------//	
	public function addBooking(Booking $booking){
	
	//-----------------------------------------------------------------UPDATE DATE-----------------------------------------------------------------//
	
		 $reqDel=$this->_db->prepare('DELETE FROM booking WHERE dt_end=:dt_end > CURDATE()');
			
			 $reqDel->execute(array(':dt_end'=>$booking->getDtEnd()));
	
	//-----------------------------------------------------------------COUNT ROOM OCCUPIED-----------------------------------------------------------------//
			$reqA=$this->_db->prepare('SELECT COUNT(*) AS nbRoomOccupied FROM booking WHERE name_hotel=:name_hotel 
													AND  :dt_begin BETWEEN dt_begin  AND dt_end
													OR  :dt_end BETWEEN dt_begin  AND dt_end');
			
			$reqA->execute(array(':name_hotel'=>$booking->getNameHotel(),
											':dt_begin'=>$booking->getDtBegin(), 
											':dt_end'=>$booking->getDtEnd(), 	
											));
			
			$rowA=$reqA->fetch();
			$nbRoomOccupied=$rowA['nbRoomOccupied'];
			
	//-----------------------------------------------------------------COUNT ROOM HOTEL-----------------------------------------------------------------//	
			$reqB=$this->_db->prepare('SELECT number_room FROM hotels WHERE name_hotels=:name_hotel');
			
			$reqB->execute(array(':name_hotel'=>$booking->getNameHotel()));
			
			$rowB=$reqB->fetch();
			$nbRoom=$rowB['number_room'];
			
	
	//-----------------------------------------------------------------BOOKING -----------------------------------------------------------------//
			//-----------------------------------------------------------------SI L'HOTEL EST VIDE-----------------------------------------------------------------//
			if($nbRoomOccupied==0){
			
			$reqC=$this->_db->prepare('INSERT INTO booking(name_guest, email, name_hotel, dt_begin, dt_end, room_number, dt_creation) 
													VALUES (:name_guest, :email, :name_hotel, :dt_begin, :dt_end, :room_number, :dt_creation)');
													
			$reqC->execute(array(':name_guest'=>htmlspecialchars($booking->getNameGuest()),
										   ':email'=>htmlspecialchars($booking->getEmail()),
										   ':name_hotel'=>$booking->getNameHotel(), 										   
										   ':dt_begin'=>$booking->getDtBegin(), 
										   ':dt_end'=>$booking->getDtEnd(), 
										   ':room_number'=>'1',
										   ':dt_creation'=>$booking->getDtCreation()									   
														));
		  echo "Votre réservation à bien été enregistré pour l'hôtel ".$booking->getNameHotel()." .Votre numéro de chambre est le 1.";
	}
	
			//-----------------------------------------------------------------ALORS SI L'HOTEL EST COMPLET-----------------------------------------------------------------//
	elseif($nbRoomOccupied==$nbRoom)
	{
	echo "L'hôtel ".$booking->getNameHotel()." est complet, pour cette période !";
	}
	
			//-----------------------------------------------------------------ALORS IL RESTE DE LA PLACE-----------------------------------------------------------------//
	else
	{
			$reqD=$this->_db->prepare('SELECT MAX(room_number) AS number_roomMax FROM booking WHERE name_hotel=:name_hotel');
			
			$reqD->execute(array(':name_hotel'=>$booking->getNameHotel()));
			
			$rowD=$reqD->fetch();
			$NewNbRoom=$rowD['number_roomMax']+1;
	
			$reqE=$this->_db->prepare('INSERT INTO booking(name_guest, email, name_hotel, dt_begin, dt_end, room_number, dt_creation) 
													VALUES (:name_guest, :email, :name_hotel, :dt_begin, :dt_end, :room_number, :dt_creation)');
													
			$reqE->execute(array(':name_guest'=>htmlspecialchars($booking->getNameGuest()),
										   ':email'=>htmlspecialchars($booking->getEmail()),
										   ':name_hotel'=>$booking->getNameHotel(), 										   
										   ':dt_begin'=>$booking->getDtBegin(), 
										   ':dt_end'=>$booking->getDtEnd(), 
										   ':room_number'=>$NewNbRoom,
										   ':dt_creation'=>$booking->getDtCreation()									   
														));
														
		  echo "Votre réservation à bien été enregistré pour l'hôtel ".$booking->getNameHotel()." .Votre numéro de chambre est le ".$NewNbRoom.".";

	}
}
//-----------------------------------------------------------------METHODS GET HOTELS-----------------------------------------------------------------//
	public function getListHotels(){
	
	$req=$this->_db->prepare('SELECT * FROM hotels');
	$req->execute();
	
	while ($row=$req->fetch()){
		echo "Nom de l'hôtel: ".$row["name_hotels"].". </br>";
		echo "Adresse de l'hôtel: ".$row["city"].". </br>";
		echo "Nombre de chambres d'hôtel: ".$row["number_room"].". </br></br>";
		}
	}
	
	public function getSelListHotels(){
	
	$req=$this->_db->prepare('SELECT * FROM hotels');
	$req->execute();
	
	while ($row=$req->fetch()){
		echo '<option value="'.$row['name_hotels']. '">'.$row['name_hotels'].'</option>';
		}
	}
}