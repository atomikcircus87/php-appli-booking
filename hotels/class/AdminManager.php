<?php

Class AdminManager {

	private $_db;
	
	public function __construct($db){
			$this->setDb($db);
	}
	
	public function setDb(PDO $db){
		$this->_db=$db;
	}
	
//-----------------------------------------------------------------METHOD ADD HOTEL-----------------------------------------------------------------//	
	public function addHotel(Admin $admin){
			
		$req=$this->_db->prepare('INSERT INTO hotels(name_hotels, city, number_room) 
												VALUES (:name_hotels, :city, :number_room)');
													
		$req->execute(array(':name_hotels'=>htmlspecialchars($admin->getNameHotel()),
									  ':city'=>htmlspecialchars($admin->getCityHotel()),
									  ':number_room'=>$admin->getNumberRoom() 										   
														));
			
		echo "L'ajout de l'hôtel ".$admin->getNameHotel()." à bien été effectué !";
	}

//-----------------------------------------------------------------METHOD DELETE HOTEL-----------------------------------------------------------------//
	public function deleteHotel(Admin $admin){
		
		$req=$this->_db->prepare('DELETE FROM hotels WHERE name_hotels=:name_hotels');
		
		$req->execute(array(':name_hotels'=>htmlspecialchars($admin->getNameHotel()),
										));
										
		echo "La suppression de l'hôtel ".$admin->getNameHotel()." à bien été effectué !";							
										
		}


//-----------------------------------------------------------------METHOD GET LIST BOOKING HOTEL-----------------------------------------------------------------//
	public function getListBooginkHotel(Admin $admin){
		
		$req=$this->_db->prepare('SELECT * FROM booking WHERE name_hotel=:name_hotels ORDER BY dt_begin ASC');
		
		$req->execute(array(':name_hotels'=>htmlspecialchars($admin->getNameHotel()),
										));
		

		while ($row=$req->fetch())
		{
		echo $line= "Le client ".$row['name_guest'].", a choisi l'hôtel ".$row['name_hotel']." et le numéro de chambre ".$row['room_number'].", et il a réservé du ".$row['dt_begin']." au ".$row['dt_end']." .<br>
					contact: ".$row['email']."<br><br>" ;
		}
		
		if(empty($line))
		{
		echo "Aucune réservation n'a eu lieu à ce jour, pour l'hôtel ".$admin->getNameHotel()." !";
		}
		}
}