<?php

Class Booking {

	protected $_id;
	protected $_name;
	protected $_email;
	protected $_hotel;
	protected $_dtBegin;
	protected $_dtEnd;
	protected $_dtCreation;
	
	protected static $error;
	
	const MSG_ERROR_NAME='NAME doit être une chaine de caractére.';
	const MSG_ERROR_EMAIL='EMAIL doit être une chaine de caractére contenant @.';
	const MSG_ERROR_DATE='DATE doit être de la forme YYYY-MM-DD.';
	const MSG_ERROR_DATE_COMPARE='La date de début de réservation doit être inférieure à la date de fin de réservation.';
	const MSG_ERROR_DATE_DAY='La date de début de réservation doit être supérieure à la date de ce jour.';
	const MSG_ERROR_END='<br>La réservation ne peut pas être créé.';
	
public function __construct(array $data){
		$this->setId($data['id']);
		$this->setNameGuest($data['name']);
		$this->setEmail($data['email']);
		$this->setNameHotel($data['hotel']);
		$this->setDtBegin($data['dt_begin']);
		$this->setDtEnd($data['dt_end']);
		$this->setDtCreation($data['dt_creation']);
		
		if(!empty(self::$error)){
			throw new Exception(self::$error.self::MSG_ERROR_END);
		}
}

//-----------------------------------------------------------------SETTERS-----------------------------------------------------------------//

public function setId($id){
	if((is_int($id)) AND ($id>0)){
	$this->_id=$id;
	}
}

public function setNameGuest($name){
	if(is_string($name)){ 
	$this->_name=$name;
	}
	else
	{
		$this->setError(self::MSG_ERROR_NAME);
	}
}

public function setEmail($email){

//---------------------UTILISATION DES EXPRESSIONS REGULIERES---------------------//

	if(preg_match('#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#',$email)){ 
	$this->_email=$email;
	}
	else
	{
		$this->setError(self::MSG_ERROR_EMAIL);
	}
}

public function setNameHotel($hotel){
	$this->_hotel=$hotel;
}

public function setDtCreation($dtCreation){
		$this->_dtCreation=$dtCreation;
}

public function setDtBegin($dtBegin){

//---------------------UTILISATION DES EXPRESSIONS REGULIERES---------------------//

	if(preg_match("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $dtBegin, $m) && checkdate($m[2], $m[3], $m[1])){
	
		if(strtotime($dtBegin)<strtotime(date("Y-m-d"))){
		
			$this->setError(self::MSG_ERROR_DATE_DAY);
			
			}
			else
			{
			
			$this->_dtBegin=$dtBegin;
			
		}
	}
	else
	{
		$this->setError(self::MSG_ERROR_DATE);
	}
}

public function setDtEnd($dtEnd){

//---------------------UTILISATION DES EXPRESSIONS REGULIERES---------------------//

	if(preg_match("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $dtEnd, $m) && checkdate($m[2], $m[3], $m[1])){
	
		if(strtotime($dtEnd)<strtotime($this->_dtBegin)){
		
		$this->setError(self::MSG_ERROR_DATE_COMPARE);
		
		}
		else
		{
		
		$this->_dtEnd=$dtEnd;
		
		}
	}
	else
	{
		$this->setError(self::MSG_ERROR_DATE);
	}
}


public function setError($msg){
	self::$error=$msg;
	}
	
//-----------------------------------------------------------------GETTERS-----------------------------------------------------------------//

public function getId(){
	return $this->_id;
	}

public function getNameGuest(){
	return $this->_name;
	}

public function getEmail(){
	return $this->_email;
	}

public function getNameHotel(){
	return $this->_hotel;
	}

public function getDtBegin(){
	return $this->_dtBegin;
	}

public function getDtEnd(){
	return $this->_dtEnd;
	}

public function getDtCreation(){
	return $this->_dtCreation;
	}

public function getError(){
	return self::$error;
	}
}