<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Site de réservations de chambres d'hôtels</title>

<link href="css/index.css" rel="stylesheet" />

</head>

<body>

	<main >

		<header>
			<h1 class="title">Site de réservations de chambres d'hôtels</h1>
		</header>
		
		<nav>
			<li><a href="#">Accueil</a></li>
			<li><a href="page_hotels/page_hotels.php">Liste des Hôtels</a></li>
			<li><a href="page_booking/page_booking.php">Formulaire de Booking</a></li>
			<li><a href="page_admin/page_admin.php">Administration</a></li>
		</nav>

		<article>
			<h2 class="welcome">Bienvenue</h2>

			<section>
				Eodem tempore Serenianus ex duce, cuius ignavia populatam in Phoenice Celsen ante rettulimus,
				pulsatae maiestatis imperii reus iure postulatus ac lege, incertum qua potuit suffragatione absolvi, 
				aperte convictus familiarem suum cum pileo, quo caput operiebat, incantato vetitis artibus ad templum misisse fatidicum,
				quaeritatum expresse an ei firmum portenderetur imperium, ut cupiebat, et cunctum.
				<br/>
				<br/>
				Sed fruatur sane hoc solacio atque hanc insignem ignominiam, quoniam uni praeter se inusta sit, 
				putet esse leviorem, dum modo, cuius exemplo se consolatur, eius exitum expectet, 
				praesertim cum in Albucio nec Pisonis libidines nec audacia Gabini fuerit ac tamen hac una plaga conciderit, ignominia senatus.
				<br/>
				<br/>
				Sed cautela nimia in peiores haeserat plagas, 
				ut narrabimus postea, aemulis consarcinantibus insidias graves apud Constantium, 
				cetera medium principem sed siquid auribus eius huius modi quivis infudisset ignotus,
				acerbum et inplacabilem et in hoc causarum titulo dissimilem sui.
			</section>
			
		</article>    
		
	</main>
	
</body>

</html>
