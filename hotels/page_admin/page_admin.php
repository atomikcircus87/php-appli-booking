<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Site de réservations de chambres d'hôtels</title>

<link href="../css/index.css" rel="stylesheet" />

</head>

<body>

	<main >

		<header>
			<h1 class="title">Site de réservations de chambres d'hôtels</h1>
		</header>
		
		<nav>
			<li><a href="../index.php">Accueil</a></li>
			<li><a href="../page_hotels/page_hotels.php">Liste des Hôtels</a></li>
			<li><a href="../page_booking/page_booking.php">Formulaire de Booking</a></li>
			<li><a href="#">Administration</a></li>
		</nav>

		<article>
			<h2 class="welcome">Administration du site de booking</h2>
			
			<section>
			
				<form method="post" name="formAdmin" action="#">
					<fieldset>
					   <legend>Ajouter un hôtel</legend>
						
						<p>Nom de l'hôtel:</p>
							<input type="text" name="nameHotel">
						
						<p>Adresse de l'hôtel:</p>
							<input type="text" name="cityHotel">
							
						<p>Nombre de chambre de l'hôtel:</p>
							<input type="text" name="numberRoom">
						<br>
						<br>
						<input type="submit" name="submitFormAddHotel" value="Valider">
							
						</fieldset>
						
						<fieldset>
						
					   <legend>Supression d'un hôtel</legend>
					   
					   <select name="selDelHotel">
								<?php
								require('../process/process_classBooking.php');
								require('../process/process_selhotels.php');
								?>	
							</select>
							<br>
							<br>
							<input type="submit" name="submitFormDeleteHotel" value="Supprimer l'hôtel">
							
						</fieldset>
						
						<fieldset>
						
					   <legend>Voir les réservations d'un hôtel</legend>
							
							<select name="selListHotel">
								<?php
								require('../process/process_selhotels.php');
								?>	
							</select>
							<br>
							<br>
							<input type="submit" name="submitFormListHotel" value="Voir les réservations">
							
							
						
						</fieldset>
						
				</form>
				<?php
							require('../process/process_admin.php');
							?>
			</section>
			
		</article>    
		
	</main>
	
</body>

</html>
