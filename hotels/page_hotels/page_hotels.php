<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Site de réservations de chambres d'hôtels</title>

<link href="../css/index.css" rel="stylesheet" />

</head>

<body>

	<main >

		<header>
			<h1 class="title">Site de réservations de chambres d'hôtels</h1>
		</header>
		
		<nav>
			<li><a href="../index.php">Accueil</a></li>
			<li><a href="#">Liste des Hôtels</a></li>
			<li><a href="../page_booking/page_booking.php">Formulaire de Booking</a></li>
			<li><a href="../page_admin/page_admin.php">Administration</a></li>
		</nav>

		<article>
			<h2 class="welcome">Liste des hôtels</h2>
			
			<section>
				<?php
					require('../process/process_hotels.php');
				?>
			</section>
			
		</article>    
		
	</main>
	
</body>

</html>
